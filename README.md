# Twitsplity

Twitsplity is sample app. It have only one feature that user input a message into textbox and click on "sent" button, then the message will be sent with some condition:

1/ If a user's input is less than or equal to 50 characters, post it as is.

2/ If a user's input is greater than 50 characters, split it into chunks that each is less than or equal to 50 characters and post each chunk as a separate message.

3/ Messages will only be split on whitespace. If the message contains a span of non-whitespace characters longer than 50 characters, display an error.

# Documentation

- using SpliteMessageModel class call sentMessage(Editable message1) to split message to many chunks!

@Param message is from EditText.getText

@return is string list after split message.
- public List<String> sentMessage(Editable message)