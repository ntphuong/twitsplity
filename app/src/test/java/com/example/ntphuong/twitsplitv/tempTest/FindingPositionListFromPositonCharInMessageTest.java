package com.example.ntphuong.twitsplitv.tempTest;

import com.example.ntphuong.twitsplitv.libraly.solution2.Chunk;
import com.example.ntphuong.twitsplitv.libraly.solution2.FindingPositionListFromPositonCharInMessage;
import com.example.ntphuong.twitsplitv.libraly.solution2.ManagementChunk;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by ntphuong on 4/7/18.
 */

public class FindingPositionListFromPositonCharInMessageTest {
    ManagementChunk chunk;

    @Before
    public void init() {
        //nguyen thanh phuong
        Chunk a = new Chunk(7);
        Chunk b = new Chunk(13);
        Chunk c = new Chunk(19);

        List<Chunk> list = new ArrayList<>();
        list.add(a);
        list.add(b);
        list.add(c);

        chunk = new ManagementChunk();
        chunk.getChunks().addAll(list);
    }

    @Test
    public void testGettingFirst() {
        int find = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(chunk.getChunks(), 6);
        assertEquals(find, 0);
    }

    @Test
    public void testGettingSecond() {
        int find = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(chunk.getChunks(), 10);
        assertEquals(find, 1);
    }

    @Test
    public void testGettingThird() {
        int find = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(chunk.getChunks(), 14);
        assertEquals(find, 2);
    }

    @Test
    public void testGettingWrongParams() {
        int find = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(chunk.getChunks(), 20);
        assertEquals(find, -1);
    }
}
