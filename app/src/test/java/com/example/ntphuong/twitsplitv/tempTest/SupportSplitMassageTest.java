package com.example.ntphuong.twitsplitv.tempTest;

import com.example.ntphuong.twitsplitv.libraly.solution1.SupportSplitMessage;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class SupportSplitMassageTest {
    /** latest*/
    @Test
    public void testLatest() {
        CharSequence charSequence = "Phuong nguyen Thanh ";
        int positionWhitespace = SupportSplitMessage.positionLatestWhiteSpace(charSequence);
        assertEquals(positionWhitespace, charSequence.length() -1);
    }
    /**first*/
    @Test
    public void testFirst() {
        CharSequence charSequence = " dfdggdgdgdgdfgdgdggdgdgdgdgdggd";
        int positionWhitespace = SupportSplitMessage.positionLatestWhiteSpace(charSequence);
        assertEquals(positionWhitespace, 0);
    }
    /**middle*/
    @Test
    public void testMiddle() {
        CharSequence charSequence = " 123456789 dgdggdgdgdgdgdggd";
        int positionWhitespace = SupportSplitMessage.positionLatestWhiteSpace(charSequence);
        assertEquals(positionWhitespace, 10);
    }
    /**none*/
    @Test
    public void testNone() {
        CharSequence charSequence = "h123456789gdgdggdgdgdgdgdggd";
        int positionWhitespace = SupportSplitMessage.positionLatestWhiteSpace(charSequence);
        assertEquals(positionWhitespace, -1);
    }

    @Test
    public void testCutMessageBelong() {
        CharSequence charSequence = "0123456 789";
        CharSequence charSequence1 = SupportSplitMessage.getMessageAfterCut(8, charSequence);
        assertEquals(charSequence1, "789");
    }

    @Test
    public void testCutMessageResultNull() {
        CharSequence charSequence = "0123456 789";
        CharSequence charSequence1 = SupportSplitMessage.getMessageAfterCut(6, charSequence);
        assertEquals(charSequence1, null);
    }

    @Test
    public void testCutMessageWithManyWhitespace() {
        CharSequence charSequence = "012 3456 789";
        CharSequence charSequence1 = SupportSplitMessage.getMessageAfterCut(8, charSequence);
        assertEquals(charSequence1, "789");
    }

    @Test
    public void cutMessageResultArray() {
        CharSequence charSequence = "012 3456 789";
        CharSequence[] charSequences = SupportSplitMessage.getMessageAfterCutResultArray(8, charSequence);
        assertEquals(charSequences[0], "012 3456 ");
        assertEquals(charSequences[1], "789");
    }
}
