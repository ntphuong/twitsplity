package com.example.ntphuong.twitsplitv.tempTest;

import com.example.ntphuong.twitsplitv.libraly.solution2.ManagementChunk;
import com.example.ntphuong.twitsplitv.libraly.solution2.StatusEvent;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by ntphuong on 4/7/18.
 */

public class AddEventTest {
    ManagementChunk chunk;

    @Before
    public void init() {
        chunk = new ManagementChunk();
    }

    @Test
    public void testOnChar() {
        chunk.input(StatusEvent.ADD, "1", 0, 0);
        chunk.input(StatusEvent.ADD, "2", 1, 0);
        chunk.input(StatusEvent.ADD, "3", 2, 0);
        chunk.input(StatusEvent.ADD, "4", 3, 0);
        chunk.input(StatusEvent.ADD, "5", 4, 0);
        chunk.input(StatusEvent.ADD, " ", 5, 0);
        chunk.input(StatusEvent.ADD, "6", 6, 0);
        chunk.input(StatusEvent.ADD, "7", 7, 0);
        chunk.input(StatusEvent.ADD, "8", 8, 0);
        chunk.input(StatusEvent.ADD, "9", 9, 0);
        chunk.input(StatusEvent.ADD, " ", 10, 0);
        assertEquals(chunk.getChunks().size(), 3);
        assertEquals(chunk.getChunks().get(0).getlength(), 6);
        assertEquals(chunk.getChunks().get(1).getlength(), 11);
        assertEquals(chunk.getChunks().get(2).getlength(), 0);

    }

    @Test
    public void testaManyChar() {
        chunk.input(StatusEvent.ADD, "0123456 89", 0, 0);
        chunk.input(StatusEvent.ADD, "012345 78 ", 10, 0);
        chunk.input(StatusEvent.ADD, " 399", 20, 0);
        assertEquals(chunk.getChunks().size(), 5);
        assertEquals(chunk.getChunks().get(0).getlength(), 8);
        assertEquals(chunk.getChunks().get(1).getlength(), 17);
        assertEquals(chunk.getChunks().get(2).getlength(), 20);
        assertEquals(chunk.getChunks().get(3).getlength(), 21);
        assertEquals(chunk.getChunks().get(4).getlength(), 0);
    }

    @Test
    public void test2White() {
        chunk.input(StatusEvent.ADD, " ", 0, 0);
        chunk.input(StatusEvent.ADD, "  ", 1, 0);

    }

    @Test
    public void testAddMiddleText() {
        chunk.input(StatusEvent.ADD, "1", 0, 0);
        chunk.input(StatusEvent.ADD, "2", 1, 0);
        chunk.input(StatusEvent.ADD, "3", 2, 0);
        chunk.input(StatusEvent.ADD, "4", 3, 0);
        chunk.input(StatusEvent.ADD, "5", 4, 0);
        chunk.input(StatusEvent.ADD, " ", 5, 0);
        chunk.input(StatusEvent.ADD, "6", 6, 0);
        chunk.input(StatusEvent.ADD, "7", 7, 0);
        chunk.input(StatusEvent.ADD, "8", 8, 0);
        chunk.input(StatusEvent.ADD, "9", 9, 0);
        chunk.input(StatusEvent.ADD, " ", 10, 0);


        chunk.input(StatusEvent.ADD, "phuong ", 1, 0);
    }
}
