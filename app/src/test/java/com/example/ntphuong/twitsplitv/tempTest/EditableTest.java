package com.example.ntphuong.twitsplitv.tempTest;



import android.text.Editable;
import android.text.SpannableStringBuilder;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EditableTest {
    SpannableStringBuilder editable;

    @Before
    public void createEdiableTest() {
        editable = mock(SpannableStringBuilder.class);

    }

    @Test
    public void insert() {
        editable.append("phuong");
        editable.insert(0, "nguyen");
        verify(editable).append("phuong");
        verify(editable).insert(0, "nguyen");
        assertEquals(editable.toString(),"nguyenphuong");
    }
}
