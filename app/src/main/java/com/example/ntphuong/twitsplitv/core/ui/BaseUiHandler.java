package com.example.ntphuong.twitsplitv.core.ui;

import android.support.annotation.LayoutRes;
import android.view.View;

public interface BaseUiHandler {
    View inflateView(@LayoutRes int layout);
}
