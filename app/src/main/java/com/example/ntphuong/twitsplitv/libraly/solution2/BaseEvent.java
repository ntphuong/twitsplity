package com.example.ntphuong.twitsplitv.libraly.solution2;

public abstract class BaseEvent {
    protected ManagementChunk managementChunk;

    BaseEvent(ManagementChunk managementChunk) {
        this.managementChunk = managementChunk;
    }

    abstract void excuse(CharSequence sequence, int start, int end);

    abstract void done();
}
