package com.example.ntphuong.twitsplitv.libraly.solution1;

import android.text.Editable;

public interface ISplitMessage {
    int length();
    Editable getEditable();

    void excuse(EventChangeMessage event, CharSequence charSequence, int start, int end);

    void setLastestPositionOfWhitespace(int lastestPositionOfWhitespace);

    void setBlank(int blank);

    int getLastestPositionOfWhitespace();

    int getBlank();

    void deleteRedundant();

    void addForFullASplits(CharSequence sequence);

    CharSequence getRedundant();

    void insertForFullASplits(CharSequence sequence, int start);
}
