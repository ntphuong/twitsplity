package com.example.ntphuong.twitsplitv.doman;


import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class Navigator {
    @IdRes
    private int fragmentId;
    private FragmentManager fragmentManager;

    private Navigator() {}

    public Navigator(@IdRes int fragmentId, FragmentManager fragmentManager) {
        if (fragmentId == 0 || fragmentManager == null) {
            throw new NullPointerException("inputting id and FragmentManager need is real value");
        }
        this.fragmentId = fragmentId;
        this.fragmentManager = fragmentManager;
    }

    public void open(Fragment fragment) {
        fragmentManager.beginTransaction().add(fragmentId, fragment).
                addToBackStack(fragment.getClass().getName()).commit();
    }
}
