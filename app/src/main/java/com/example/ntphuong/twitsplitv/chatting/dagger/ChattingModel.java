package com.example.ntphuong.twitsplitv.chatting.dagger;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import com.example.ntphuong.twitsplitv.chatting.ui.MessageViewModel;
import com.example.ntphuong.twitsplitv.chatting.ui.SpliteMessageModel;

import dagger.Module;

@Module
public class ChattingModel {
    Fragment fragment;

    public ChattingModel(Fragment fragment) {
        this.fragment = fragment;
    }

    public MessageViewModel getMessageViewModel() {
        return ViewModelProviders.of(fragment).get(MessageViewModel.class);
    }

    public SpliteMessageModel spliteMessageModel() {
        return ViewModelProviders.of(fragment).get(SpliteMessageModel.class);
    }
}
