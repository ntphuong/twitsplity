package com.example.ntphuong.twitsplitv.chatting.dagger;

import com.example.ntphuong.twitsplitv.chatting.ui.ChattingFragment;

import dagger.Subcomponent;

@Subcomponent(modules = ChattingModel.class)
@ChattingScope
public interface ChattingComponent {
    void inject(ChattingFragment fragment);
}
