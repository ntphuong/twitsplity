package com.example.ntphuong.twitsplitv.core.dagger;

import com.example.ntphuong.twitsplitv.chatting.dagger.ChattingComponent;
import com.example.ntphuong.twitsplitv.chatting.dagger.ChattingModel;
import com.example.ntphuong.twitsplitv.core.ui.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {NavigatorModule.class})
@Singleton
public interface MainCommpont {
    void inject(MainActivity mainActivity);
    ChattingComponent chattingComponent(ChattingModel chattingModel);
}
