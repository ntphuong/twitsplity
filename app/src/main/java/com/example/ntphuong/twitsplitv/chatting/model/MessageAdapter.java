package com.example.ntphuong.twitsplitv.chatting.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ntphuong.twitsplitv.R;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    List<String> messages;
    List<String> page;

    public MessageAdapter() {
        messages = new ArrayList<>();
        page = new ArrayList<>();
    }

    public void addMessage(String message) {
        this.messages.add(message);
        notifyItemChanged(this.messages.size() - 1);
    }

    public void update(List<String> messages) {
        int length = this.messages.size();
        this.messages.addAll(messages);
        if (messages.size() > 1) {
            int lengthMessage = messages.size();
            for (int i = 0; i < messages.size(); i++) {
                int numeberPage = i + 1;
                page.add("(" + numeberPage + "/" + lengthMessage + ")");
            }
        } else {
            page.add("");
        }
        notifyItemChanged(length, length + messages.size());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_message_chatting, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateMessage(this.messages.get(position), page.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView message;

        public ViewHolder(View itemView) {
            super(itemView);
            message = itemView.findViewById(R.id.message);
        }

        public void updateMessage(String message, String page) {
            this.message.setText(message + " " + page);
        }
    }
}
