package com.example.ntphuong.twitsplitv.libraly.solution1;

import android.text.Spanned;

public interface IManagementSplitMessage {
    void filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend);
}
