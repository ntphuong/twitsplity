package com.example.ntphuong.twitsplitv.chatting.ui;

import android.arch.lifecycle.ViewModel;
import android.text.Editable;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class SpliteMessageModel extends ViewModel{
    private Editable message;

    @Inject
    public SpliteMessageModel() {
    }

    public List<String> sentMessage(Editable message1) {
        this.message = message1;
        List<String> splitMessage = new ArrayList<>();
        while (message.length() > 50) {
            String sub = message.subSequence(0, 49).toString();
            int positionNeedText = sub.lastIndexOf(' ');
            if (positionNeedText == -1) {
                return null;
            } else {
                splitMessage.add(sub.substring(0, positionNeedText).trim());
                message.delete(0, positionNeedText + 1);
            }
        }
        splitMessage.add(message.toString().trim());
        return splitMessage;
    }
}
