package com.example.ntphuong.twitsplitv.libraly.solution1;

public class DeleteEvent extends IEventChangeMessage {

    DeleteEvent(ISplitMessage splitMessage) {
        super(splitMessage);
    }

    @Override
    public EventChangeMessage getStatus() {
        return EventChangeMessage.DELETE;
    }

    @Override
    boolean excuse() {
        splitMessage.getEditable().delete(start, end);
        return true;
    }
}
