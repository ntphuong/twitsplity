package com.example.ntphuong.twitsplitv.core.dagger;

import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;

import com.example.ntphuong.twitsplitv.doman.Navigator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class NavigatorModule {
    private Navigator navigator;

    public NavigatorModule(@IdRes int fragmentId, FragmentManager fragmentManager) {
        navigator = new Navigator(fragmentId, fragmentManager);
    }

    @Provides
    @Singleton
    public Navigator getNavigator() {
        return navigator;
    }
}
