package com.example.ntphuong.twitsplitv.core.ui;

import android.os.Bundle;

import com.example.ntphuong.twitsplitv.R;
import com.example.ntphuong.twitsplitv.chatting.ui.ChattingFragment;
import com.example.ntphuong.twitsplitv.core.dagger.DaggerMainCommpont;
import com.example.ntphuong.twitsplitv.core.dagger.MainCommpont;
import com.example.ntphuong.twitsplitv.core.dagger.NavigatorModule;
import com.example.ntphuong.twitsplitv.doman.Navigator;

import javax.inject.Inject;


public class MainActivity extends BaseActivity {
    private MainCommpont component;
    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getComponent().inject(this);
        navigator.open(ChattingFragment.newInstance());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public MainCommpont getComponent() {
        if (component == null) {
            component = DaggerMainCommpont.builder().navigatorModule(new NavigatorModule(
                    R.id.main_content, getSupportFragmentManager())).build();
        }
        return component;
    }
}
