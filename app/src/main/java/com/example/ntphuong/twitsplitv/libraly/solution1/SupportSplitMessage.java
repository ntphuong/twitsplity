package com.example.ntphuong.twitsplitv.libraly.solution1;

public class SupportSplitMessage {
    public static int positionLatestWhiteSpace(CharSequence charSequence) {
        for (int i = charSequence.length() - 1; i >= 0; i--) {
            if (charSequence.charAt(i) == ' ') {
                return i;
            }
        }
        return 0;
    }

    public static CharSequence getMessageAfterCut(int number, CharSequence charSequence) {
        int positionWhiteSpace = -1;
        for (int i = 0; i <= number; i++) {
            char index = charSequence.charAt(i);
            if (index == ' ') {
                positionWhiteSpace = i;
            }
        }
        if (positionWhiteSpace > -1) {
            return charSequence.subSequence(positionWhiteSpace + 1, charSequence.length());
        } else {
            return null;
        }
    }

    public static CharSequence[] getMessageAfterCutResultArray(int number, CharSequence charSequence) {
        int positionWhiteSpace = -1;
        for (int i = 0; i <= number; i++) {
            char index = charSequence.charAt(i);
            if (index == ' ') {
                positionWhiteSpace = i;
            }
        }
        if (positionWhiteSpace > -1) {
            CharSequence b = charSequence.subSequence(positionWhiteSpace + 1, charSequence.length());
            CharSequence a = charSequence.subSequence(0, positionWhiteSpace + 1);
            return new CharSequence[] {a, b};
        } else {
            return null;
        }
    }
}
