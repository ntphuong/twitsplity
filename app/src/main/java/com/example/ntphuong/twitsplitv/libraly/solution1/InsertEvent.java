package com.example.ntphuong.twitsplitv.libraly.solution1;

public class InsertEvent extends IEventChangeMessage {
    InsertEvent(ISplitMessage splitMessage) {
        super(splitMessage);
    }

    @Override
    EventChangeMessage getStatus() {
        return EventChangeMessage.INSERT;
    }

    @Override
    boolean excuse() {
        if (sequence.length() == 0) {
            return true;
        }
        if (sequence.length() > 1) {
            if (splitMessage.length() + sequence.length() > Config.MAX_LENGTH_MESSAGE) {
                splitMessage.insertForFullASplits(sequence, start);
            } else {
                int lastPositionWhitespace = Math.max(start + SupportSplitMessage.positionLatestWhiteSpace(sequence), splitMessage.getLastestPositionOfWhitespace());
                splitMessage.setLastestPositionOfWhitespace(lastPositionWhitespace);
                splitMessage.getEditable().insert(start, sequence);
            }
        } else {
            if (splitMessage.length() + 1 > Config.MAX_LENGTH_MESSAGE) {
                splitMessage.deleteRedundant();
                return false;
            }
            if (sequence.equals(" ")) {
                int lastPositionWhitespace = Math.max(start, splitMessage.getLastestPositionOfWhitespace());
                splitMessage.setLastestPositionOfWhitespace(lastPositionWhitespace);
            }

            splitMessage.getEditable().insert(start, sequence);
        }
        return true;
    }
}
