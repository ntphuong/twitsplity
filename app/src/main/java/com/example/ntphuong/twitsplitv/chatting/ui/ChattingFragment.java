package com.example.ntphuong.twitsplitv.chatting.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

import com.example.ntphuong.twitsplitv.R;
import com.example.ntphuong.twitsplitv.chatting.dagger.ChattingComponent;
import com.example.ntphuong.twitsplitv.chatting.dagger.ChattingModel;
import com.example.ntphuong.twitsplitv.chatting.model.MessageAdapter;
import com.example.ntphuong.twitsplitv.chatting.model.MyItemDecoration;
import com.example.ntphuong.twitsplitv.core.ui.BaseFragment;
import com.example.ntphuong.twitsplitv.core.ui.MainActivity;
import com.example.ntphuong.twitsplitv.databinding.FragmentChattingBinding;

import java.util.List;

import javax.inject.Inject;

public class ChattingFragment extends BaseFragment<FragmentChattingBinding> {
    @Inject
    MessageViewModel messageViewModel;
    @Inject
    SpliteMessageModel spliteMessageModel;
    MessageAdapter adapter;

    private ChattingComponent component;

    public static final String TAG = ChattingFragment.class.getName();

    public static ChattingFragment newInstance() {
        Bundle args = new Bundle();
        ChattingFragment fragment = new ChattingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
    }

    @Override
    protected int getLayoutView() {
        return R.layout.fragment_chatting;
    }

    @Override
    protected void setupCreateView() {
        setupEvent();
        setupRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupRecyclerView() {
        adapter = new MessageAdapter();
        binding.listMessage.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.listMessage.setAdapter(adapter);
        binding.listMessage.addItemDecoration(new MyItemDecoration());
    }

    private void setupEvent() {
        binding.sentButton.setOnClickListener(v -> {
            List<String> messages =  spliteMessageModel.sentMessage(binding.textMessage.getText());
            if (messages == null) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_bigger_50_character_for_a_word), Toast.LENGTH_LONG).show();;
            } else {
                adapter.update(messages);
                binding.textMessage.setText("");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (messageViewModel != null && messageViewModel.message != null) {
            binding.textMessage.setText(messageViewModel.message);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        messageViewModel.message = binding.textMessage.getText().toString();
    }

    public ChattingComponent getComponent() {
        if (component == null) {
            component = ((MainActivity) getActivity()).getComponent().chattingComponent(new ChattingModel(this));
        }
        return component;
    }
}
