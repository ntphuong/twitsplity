package com.example.ntphuong.twitsplitv.libraly.solution1;

import android.text.Spanned;

import java.util.ArrayList;
import java.util.List;

public class ManagementSplitMessage implements IManagementSplitMessage {
    List<SplitMessage> messages;
    SplitMessage currentMessage;

    ManagementSplitMessage() {
        this.messages = new ArrayList<>();
        currentMessage = new SplitMessage();
    }

    /**
     * listen event user input message
     * @param source character user input
     * @param start position start of the source
     * @param end position end of the souce
     * @param dest old message before change
     * @param dstart position start change of the old string
     * @param dend position end change of the old string
     * @*/
    @Override
    public void filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

    }
}
