package com.example.ntphuong.twitsplitv.libraly.solution2;

public class DelecteEvent extends BaseEvent{

    DelecteEvent(ManagementChunk managementChunk) {
        super(managementChunk);
    }

    @Override
    void excuse(CharSequence sequence, int start, int end) {
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            if (c == ' ') {
                managementChunk.append(start);
            } else {
                int position = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(managementChunk.getChunks(), start);
                managementChunk.minus(position);
            }
        }
    }

    @Override
    void done() {

    }
}
