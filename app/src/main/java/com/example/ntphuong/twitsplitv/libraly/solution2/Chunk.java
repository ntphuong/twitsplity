package com.example.ntphuong.twitsplitv.libraly.solution2;


public class Chunk {
    private int length;

    public Chunk(int length) {
        this.length = length;
    }

    public int getlength() {
        return length;
    }

    public void setlength(int length) {
        this.length = length;
    }

    public void plusMoreOne() {
        this.length++;
    }

    public void minusMoreOne() {
        this.length--;
    }

    public void plusMore(int length) {
        this.length+=length;
    }
}
