package com.example.ntphuong.twitsplitv.libraly.solution2;

import java.util.ArrayList;
import java.util.List;

public class ManagementChunk {
    List<Chunk> chunks;
    int currentLength = 0;
    AddEvent addEvent;

    public ManagementChunk() {
        this.chunks = new ArrayList<>();
        addEvent = new AddEvent(this);
        this.chunks.add(new Chunk(0));
    }

    public List<Chunk> getChunks() {
        return chunks;
    }

    public void append(int position) {
        int position2 = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(chunks, position);
        if (position2 == 0) {
            minus(0);
        } else {
            Chunk chunk1 = chunks.get(position2 - 1);
            Chunk chunk2 = chunks.get(position2);
            chunk1.setlength(chunk2.getlength());
            chunks.remove(chunk2);
        }
    }

    public void minus(int position) {
        for (int i = position; i < chunks.size(); i++) {
            Chunk chunk = chunks.get(i);
            chunk.minusMoreOne();
        }
    }

    public void input(StatusEvent event, CharSequence charSequence, int start, int end) {
        switch (event) {
            case ADD:
                addEvent.excuse(charSequence, start, end);
                break;
        }
    }

    public void changePosition() {
        chunks.get(currentLength).setlength(currentLength);
        currentLength =0 ;
    }
}
