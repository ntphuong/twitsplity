package com.example.ntphuong.twitsplitv.libraly.solution1;

import android.text.Editable;

import java.lang.ref.SoftReference;

public class SplitMessage implements ISplitMessage{
    Editable editable;
    int lastestPositionOfWhitespace;
    int blank = -1;
    CharSequence redundant;

    SoftReference<IEventChangeMessage>  addEvent;
    SoftReference<IEventChangeMessage> deleteEvent;
    SoftReference<IEventChangeMessage> replaceEvent;
    SoftReference<IEventChangeMessage> insertEvent;

    public SplitMessage() {
        editable = Editable.Factory.getInstance().newEditable("");
        addEvent = new SoftReference<IEventChangeMessage>(new PlusEvent(this));
        deleteEvent = new SoftReference<IEventChangeMessage>(new DeleteEvent(this));
        replaceEvent = new SoftReference<IEventChangeMessage>(new ReplaceEvent(this));
        insertEvent = new SoftReference<IEventChangeMessage>(new InsertEvent(this));
        editable = Editable.Factory.getInstance().newEditable("");
    }

    @Override
    public int length() {
        return editable.length();
    }

    @Override
    public Editable getEditable() {
        return editable;
    }

    @Override
    public void excuse(EventChangeMessage event, CharSequence charSequence, int start, int end) {
        switch (event) {
            case ADD:
                addEvent.get().setValue(charSequence, start, end);
                addEvent.get().excuse();
                break;
            case DELETE:
                deleteEvent.get().setValue(charSequence, start, end);
                deleteEvent.get().excuse();
                break;
            case INSERT:
                insertEvent.get().setValue(charSequence, start, end);
                insertEvent.get().excuse();
                break;
            case REPLACE:
                replaceEvent.get().setValue(charSequence, start, end);
                replaceEvent.get().excuse();
                break;
        }
    }

    @Override
    public void setLastestPositionOfWhitespace(int lastestPositionOfWhitespace) {
        this.lastestPositionOfWhitespace = lastestPositionOfWhitespace;
    }

    @Override
    public void setBlank(int blank) {
        this.blank = blank;
    }

    @Override
    public int getLastestPositionOfWhitespace() {
        return lastestPositionOfWhitespace;
    }

    @Override
    public int getBlank() {
        return blank;
    }

    /**
     * use for case that add message but it is too long, need cut from whitespace to end*/
    @Override
    public void deleteRedundant() {
        redundant = editable.subSequence(lastestPositionOfWhitespace, editable.length());
        editable.delete(lastestPositionOfWhitespace, editable.length());
    }

    @Override
    public void addForFullASplits(CharSequence sequence) {
        int index = Config.MAX_LENGTH_MESSAGE - length();
        CharSequence[] charSequences = SupportSplitMessage.getMessageAfterCutResultArray(index, sequence);
        if (charSequences != null && charSequences.length >= 2) {
            CharSequence first = charSequences[0];
            CharSequence second = charSequences[1];
            editable.append(first);
            redundant = second;
        }
        redundant = sequence;
    }

    public CharSequence getRedundant() {
        return redundant;
    }

    @Override
    public void insertForFullASplits(CharSequence sequence, int start) {
        int index = Config.MAX_LENGTH_MESSAGE - start;
        CharSequence[] charSequences = SupportSplitMessage.getMessageAfterCutResultArray(index, sequence);
        if (charSequences != null && charSequences.length >= 2) {
            CharSequence first = charSequences[0];
            CharSequence seconds = charSequences[1];
            editable.insert(start, first);
            editable.insert(start + first.length(), seconds);
            redundant = editable.subSequence(start + first.length(), length());
            editable.delete(start + first.length(), length());
        }
        redundant = Editable.Factory.getInstance().newEditable(sequence).append(editable.subSequence(start, length()));
    }
}
