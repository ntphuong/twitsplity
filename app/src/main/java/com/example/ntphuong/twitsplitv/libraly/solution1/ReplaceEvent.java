package com.example.ntphuong.twitsplitv.libraly.solution1;

public class ReplaceEvent extends IEventChangeMessage{
    ReplaceEvent(ISplitMessage splitMessage) {
        super(splitMessage);
    }

    @Override
    EventChangeMessage getStatus() {
        return EventChangeMessage.REPLACE;
    }

    @Override
    boolean excuse() {
        splitMessage.getEditable().replace(start, end, sequence);
        return true;
    }
}
