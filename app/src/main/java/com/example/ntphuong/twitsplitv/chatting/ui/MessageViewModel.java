package com.example.ntphuong.twitsplitv.chatting.ui;

import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

public class MessageViewModel extends ViewModel{
    String message;

    @Inject
    public MessageViewModel() {

    }

    public void setMessage(String message) {
        this.message = message;
    }
}
