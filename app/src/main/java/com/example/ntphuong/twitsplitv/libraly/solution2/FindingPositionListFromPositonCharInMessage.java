package com.example.ntphuong.twitsplitv.libraly.solution2;

import java.util.List;

public class FindingPositionListFromPositonCharInMessage {

    public static int getPositionOfListManagementViaPositionCharInMessage(List<Chunk> chunkList, int position) {
        for (int i = 0; i < chunkList.size(); i++) {
            if (chunkList.get(i).getlength() - position  > 0) {
                return i;
            }
        }
        return chunkList.size() - 1;
//        throw new IllegalArgumentException("position bigger than length of chunklist");
    }
}
