package com.example.ntphuong.twitsplitv.libraly.solution2;

public class AddEvent extends BaseEvent {
    AddEvent(ManagementChunk managementChunk) {
        super(managementChunk);
    }

    @Override
    void excuse(CharSequence sequence, int start, int end) {
        int leght = 0;
        for (int i = 0; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            if (c == ' ') {
                leght = managementChunk.currentLength;
                addNewChuck(start);
                start+=leght;
            } else {
                managementChunk.currentLength++;
            }
        }
    }

    @Override
    void done() {

    }

    public void addNewChuck(int position) {
        int length = FindingPositionListFromPositonCharInMessage.getPositionOfListManagementViaPositionCharInMessage(managementChunk.chunks, position);
        if (length == 0) {
            managementChunk.chunks.get(length).setlength(managementChunk.currentLength + 1);
            plus(position + 1, managementChunk.currentLength);
        } else {
            managementChunk.chunks.get(length).setlength(managementChunk.currentLength + managementChunk.chunks.get(length - 1).getlength() + 1);
        }
        int position2 = length + 1;
        managementChunk.chunks.add(position2, new Chunk(0));
        managementChunk.currentLength = 0;
    }

    private void plus(int afterPosition, int value) {
        for (int i = afterPosition; i < managementChunk.chunks.size(); i++) {
            Chunk chunk = managementChunk.chunks.get(i);
            chunk.plusMore(value);
        }
    }
}
