package com.example.ntphuong.twitsplitv.libraly.solution1;


import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.AttributeSet;
import android.util.Log;

public class SplitEditText extends AppCompatEditText {
    public static final String TAG = SplitEditText.class.getName();
    IManagementSplitMessage management;


    public SplitEditText(Context context) {
        super(context);
        init();
    }

    public SplitEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SplitEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        management = new ManagementSplitMessage();
        setFilters(new InputFilter[] {filter});
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        Log.d(TAG, "onSelectionChanged: " + selStart + " /-" + selEnd);
    }

    InputFilter filter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            Log.d(TAG, "filter: source  " + source);
            Log.d(TAG, "filter: start  " + start);
            Log.d(TAG, "filter: end  " + end);
            Log.d(TAG, "filter: dest  " + dest);
            Log.d(TAG, "filter: dstart  " + dstart);
            Log.d(TAG, "filter: dend  " + dend);
            Log.d(TAG, "filter ==================" );
            return source;
        }
    };
}
