package com.example.ntphuong.twitsplitv.libraly.solution1;

public abstract class IEventChangeMessage {
    CharSequence sequence;
    int start;
    int end;
    ISplitMessage splitMessage;
    CharSequence redundant;

    IEventChangeMessage(ISplitMessage splitMessage) {
        this.splitMessage = splitMessage;
    }

    abstract EventChangeMessage getStatus();

    void setValue(CharSequence sequence, int start, int end) {
        this.sequence = sequence;
        this.start = start;
        this.end = end;
    }

    abstract boolean excuse();;

    public CharSequence getRedundant() {
        return redundant;
    }
}
