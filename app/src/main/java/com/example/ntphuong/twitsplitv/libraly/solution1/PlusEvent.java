package com.example.ntphuong.twitsplitv.libraly.solution1;


public class PlusEvent extends IEventChangeMessage {

    PlusEvent(ISplitMessage splitMessage) {
        super(splitMessage);
    }

    @Override
    EventChangeMessage getStatus() {
        return EventChangeMessage.ADD;
    }

    @Override
    boolean excuse() {
        if (sequence.length() == 0) {
            return true;
        }
        if (sequence.length() > 1) {
            if (splitMessage.length() + sequence.length() > Config.MAX_LENGTH_MESSAGE) {
                splitMessage.addForFullASplits(sequence);
                return false;
            } else {
                splitMessage.setLastestPositionOfWhitespace(splitMessage.length() + SupportSplitMessage.positionLatestWhiteSpace(sequence));
                splitMessage.getEditable().append(sequence);
                return true;
            }
        } else {
            if (splitMessage.length() + 1 > Config.MAX_LENGTH_MESSAGE) {
                splitMessage.deleteRedundant();
                return false;
            }
            if (sequence.equals(" ")) {
                splitMessage.setLastestPositionOfWhitespace(splitMessage.length());
            }
            splitMessage.getEditable().append(sequence);
            return true;
        }
    }
}
