package com.example.ntphuong.twitsplitv;

import com.example.ntphuong.twitsplitv.libraly.solution1.EventChangeMessage;
import com.example.ntphuong.twitsplitv.libraly.solution1.SplitMessage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InsertEventTest {
    SplitMessage splitMessage;

    @Before
    public void init() {
        splitMessage = new SplitMessage();
        splitMessage.excuse(EventChangeMessage.ADD, "0123456789 0123456789 0123456789 0123456789", 0, 0);
    }



    @Test
    public void plusOverLimit() {
        splitMessage.excuse(EventChangeMessage.INSERT, " 0123456789", 11, 0);
        assertEquals(splitMessage.getRedundant(), "0123456789");
        assertEquals(splitMessage.getEditable().toString(), "0123456789 0123456789 0123456789 0123456789 ");
    }

    @Test
    public void plueOnLimit() {
        splitMessage.excuse(EventChangeMessage.INSERT, " 01234", 11, 0);
        assertEquals(splitMessage.getRedundant(), null);
        assertEquals(splitMessage.getEditable().toString(), "0123456789 0123456789 0123456789 0123456789 01234");
    }
}
