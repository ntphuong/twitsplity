package com.example.ntphuong.twitsplitv;

import android.text.Editable;

import com.example.ntphuong.twitsplitv.chatting.ui.SpliteMessageModel;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SpliteMessageModelTest {
    SpliteMessageModel spliteMessageModel;

    @Before
    public void init() {
        spliteMessageModel = new SpliteMessageModel();
    }

    @Test
    public void test() {
        String message = "123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789";
        Editable realMessage = Editable.Factory.getInstance().newEditable(message);
        List<String> messages = spliteMessageModel.sentMessage(realMessage);
        assertEquals(messages.size(), 2);
        assertEquals(messages.get(0),"123456789 123456789 123456789 123456789");
        assertEquals(messages.get(1),"123456789 123456789 123456789 123456789");
    }
}
