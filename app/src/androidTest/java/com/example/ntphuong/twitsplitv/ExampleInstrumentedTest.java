package com.example.ntphuong.twitsplitv;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.Editable;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.ntphuong.twitsplitv", appContext.getPackageName());
    }

    @Test
    public void editable() {
        Editable editable = Editable.Factory.getInstance().newEditable("");
        editable.append("phuong");
        editable.insert(1,"nguyen");
        assertEquals(editable.toString(), "pnguyenhuong");
    }
}
